<?php
error_reporting(E_ALL);
ini_set('display_errors', 1); // For debugging purposes only

include 'config.php'; // Include credentials here

require 'vendor/autoload.php'; // Composer dependencies

use iMarc\GoogleCustomSearch;

use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;

$serviceRequest = new DefaultServiceRequest( $oauth_key ); // Note oauth key will expire after an hour
ServiceRequestFactory::setInstance($serviceRequest);

// For Google Custom Search
$search = new GoogleCustomSearch( $search_id, $api_key );

$keyword = 'scholarship'; // Keyword to search
$search_page = '1'; // Search result page
$search_per_page = '10'; // Search result per page

$results = $search->search( $keyword, $search_page, $search_per_page );
$response = $results->results; // Search results


// For Google Spreadsheet
$spreadsheet_title = 'Sheets'; // The name of the spreadsheet you want to use
$worksheet_title = 'Sheet1'; // The name of the worksheet you want to use

$spreadsheetService = new Google\Spreadsheet\SpreadsheetService();
$spreadsheetFeed = $spreadsheetService->getSpreadsheetFeed();
$spreadsheet = $spreadsheetFeed->getByTitle( $spreadsheet_title );
$worksheetFeed = $spreadsheet->getWorksheetFeed();
$worksheet = $worksheetFeed->getByTitle( $worksheet_title );

$cellFeed = $worksheet->getCellFeed();
$listFeed = $worksheet->getListFeed();

if ( is_array( $response ) ) {
	foreach ( $response as $search ) {
		$listFeed->insert( ["url" => $search->link] ); // Use this to insert data to spreadsheet change "url" to your row heading slug. 
	}
}
